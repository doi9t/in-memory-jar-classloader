/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.classloader.memory.jar;

import ca.watier.classloader.memory.jar.exceptions.*;
import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.ram.RamFileProvider;

import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.util.*;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

public class InMemoryJarClassloader extends ClassLoader {
    public static final String RES_URL_SCHEME = "in-memory-classloader";
    public static final String BASE_RES_URL_SCHEME = RES_URL_SCHEME + ":";
    private static final int JAVA_SPEC_VERSION = (int) Double.parseDouble(System.getProperty("java.class.version"));
    private static final Map<Key, ClassLoader> CLASS_LOADER_MAP = Collections.synchronizedMap(new HashMap<>(50));
    private static final String CLASS_EXT = ".class";
    private static final String CLASS_EXT_REGEX = "\\.class$";
    private static final byte[] CLASS_MAGIC = {(byte) 0xCA, (byte) 0xFE, (byte) 0xBA, (byte) 0xBE};
    private final Map<String, byte[]> rawClassCache = Collections.synchronizedMap(new HashMap<>(250));

    private final Set<String> availableResourceFiles;
    private final StandardFileSystemManager standardFileSystemManager;

    private Manifest manifest;

    protected InMemoryJarClassloader(JarInputStream jarInputStream, ClassLoader parentClassloader) throws MemoryJarClassloaderException {
        super(parentClassloader);

        if (jarInputStream == null) {
            throw new InvalidJarException();
        }

        availableResourceFiles = new HashSet<>();
        standardFileSystemManager = new StandardFileSystemManager();

        try {
            standardFileSystemManager.init();
            standardFileSystemManager.addProvider(RES_URL_SCHEME, new RamFileProvider());
        } catch (FileSystemException fileSystemException) {
            throw new MemoryJarClassloaderException(fileSystemException);
        }

        loadClassesAndResources(jarInputStream);
        loadManifest(jarInputStream);
    }

    private void loadClassesAndResources(JarInputStream jarInputStream) throws MemoryJarClassloaderException {
        try {
            // Preload all the classes in a temporary map, so all the classes are present
            ZipEntry entry;
            while ((entry = jarInputStream.getNextEntry()) != null) {
                String name = entry.getName();

                FileObject fileObject = getResourceFileObject(name);

                if (entry.isDirectory() && !fileObject.exists()) {
                    fileObject.createFolder();
                    continue;
                }

                byte[] bytes = readAllBytes(jarInputStream);
                if (isClassFromBytes(bytes) && name.endsWith(CLASS_EXT)) {
                    byte[] majorVersionBytes = {bytes[6], bytes[7]};
                    int majorVersion = new BigInteger(majorVersionBytes).intValue();

                    if (majorVersion > JAVA_SPEC_VERSION) { // incompatible class, unable to load
                        continue;
                    }

                    String[] split = name.split("/");
                    int length = split.length;

                    // Multiple version of the lib, depending on the JVM version
                    if (length > 2 && "META-INF".equalsIgnoreCase(split[0]) && "versions".equalsIgnoreCase(split[1])) {
                        // remove section of array [0, 2] (META-INF/versions/[version])
                        String[] temps = new String[length - 3];
                        System.arraycopy(split, 3, temps, 0, temps.length);
                        split = temps;
                    }

                    String binaryName = String.join(".", split).replaceFirst(CLASS_EXT_REGEX, "");
                    rawClassCache.put(binaryName, bytes);
                } else {
                    availableResourceFiles.add(name);
                    fileObject.createFile();
                    FileContent objectContent = fileObject.getContent();
                    try (OutputStream outputStream = objectContent.getOutputStream()) {
                        outputStream.write(bytes);
                    }
                }
            }
        } catch (IOException | IndexOutOfBoundsException e) {
            throw new UnableToReadClassException();
        } catch (ClassFormatError | SecurityException ex) {
            throw new UnableToDefineClassException(ex);
        }
    }

    private void loadManifest(JarInputStream jarInputStream) {
        Manifest manifestOfStream =
                Optional.ofNullable(jarInputStream)
                        .map(JarInputStream::getManifest)
                        .orElse(null);

        if (manifestOfStream != null) {
            this.manifest = new Manifest(manifestOfStream);
        } else {
            this.manifest = new Manifest();
        }
    }

    private FileObject getResourceFileObject(String name) throws InvalidFileNameException, FileSystemException {
        String ramUri = buildResourceWithScheme(name);
        return standardFileSystemManager.resolveFile(ramUri);
    }

    private static byte[] readAllBytes(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[4096];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        return buffer.toByteArray();
    }

    private boolean isClassFromBytes(byte[] bytes) {
        if (bytes == null || bytes.length < 10) {
            return false;
        }

        return Arrays.equals(CLASS_MAGIC, new byte[]{bytes[0], bytes[1], bytes[2], bytes[3]});
    }

    private String buildResourceWithScheme(String name) throws InvalidFileNameException {
        if (name == null || name.isEmpty()) {
            throw new InvalidFileNameException();
        }

        if (name.startsWith("/")) {
            return BASE_RES_URL_SCHEME.concat(name);
        } else {
            return BASE_RES_URL_SCHEME.concat("/").concat(name);
        }
    }

    /**
     * Method that load the requested file from the parent classloader.
     *
     * @param file              - The file to be loaded from the classloader; if the key already exists, the previous instance will be returned, cannot be empty
     * @param parentClassloader - The parent classloader, cannot be null
     * @throws MemoryJarClassloaderException
     */
    public static synchronized ClassLoader load(String file, ClassLoader parentClassloader) throws MemoryJarClassloaderException {

        if (parentClassloader == null) {
            throw new InvalidClassloaderException();
        } else if (file == null || file.isEmpty()) {
            throw new InvalidJarException();
        }

        ClassLoader loadedClassloader = load(Key.create(file));
        if (loadedClassloader != null) {
            return loadedClassloader;
        }

        InputStream resourceAsStream = parentClassloader.getResourceAsStream(file);

        if(resourceAsStream == null) {
            // Retry with a leading slash, in case the classloader need it.
            resourceAsStream = parentClassloader.getResourceAsStream("/".concat(file));
        }

        return createInMemoryJarClassloader(parentClassloader, resourceAsStream);
    }

    /**
     * Method to load the classloader from a key
     *
     * @return null if the key is not present, the classloader otherwise
     */
    public static ClassLoader load(Key key) {
        return CLASS_LOADER_MAP.get(key);
    }

    /**
     * Create a new or fetch an already loaded instance of the classloader.
     *
     * @param parentClassloader - The parent
     * @param resourceAsStream - The stream
     * @return
     * @throws MemoryJarClassloaderException
     */
    private static InMemoryJarClassloader createInMemoryJarClassloader(ClassLoader parentClassloader, InputStream resourceAsStream) throws MemoryJarClassloaderException {
        if (resourceAsStream == null) {
            throw new UnableToReadJarException();
        }

        try {
            JarInputStream jarInputStream;
            if (resourceAsStream instanceof JarInputStream) {
                jarInputStream = (JarInputStream) resourceAsStream;
            } else {
                jarInputStream = new JarInputStream(resourceAsStream);
            }

            return new InMemoryJarClassloader(jarInputStream, parentClassloader);
        } catch (IOException e) {
            throw new UnableToReadJarException(e);
        }
    }

    /**
     * Method that load the requested file from the input stream only.
     *
     * @param key               - The key to be used; if the key already exists, the previous instance will be returned, cannot be null
     * @param jarInputStream    - The {@link JarInputStream} that will be used, cannot be null
     * @param parentClassloader - The parent classloader, can be null
     * @throws MemoryJarClassloaderException
     */
    public static synchronized ClassLoader load(Key key, JarInputStream jarInputStream, ClassLoader parentClassloader) throws MemoryJarClassloaderException {
        if (key == null || jarInputStream == null) {
            throw new InvalidJarException();
        }

        return getClassLoader(key, jarInputStream, parentClassloader);
    }

    private static ClassLoader getClassLoader(Key key, JarInputStream jarInputStream, ClassLoader parentClassloader) throws MemoryJarClassloaderException {
        ClassLoader loadedClassloader = load(key);

        if (loadedClassloader != null) {
            return loadedClassloader;
        }

        return createInMemoryJarClassloader(parentClassloader, jarInputStream);
    }

    /**
     * Method that load the requested file from the {@link JarInputStream}.
     *
     * @param file              - The path and the file that will be used, if the key already exists, the previous instance will be returned, cannot be empty
     * @param jarInputStream    - The {@link JarInputStream} that will be used, cannot be null
     * @param parentClassloader - The parent classloader, can be null
     * @throws MemoryJarClassloaderException
     */
    public static synchronized ClassLoader load(String file, JarInputStream jarInputStream, ClassLoader parentClassloader) throws MemoryJarClassloaderException {
        String correctedPath = file;

        if (jarInputStream == null) {
            throw new InvalidClassloaderException();
        }

        if (file == null || file.isEmpty()) {
            throw new InvalidJarException();
        } else if (!file.startsWith("/")) {
            correctedPath = "/".concat(correctedPath);
        }

        return getClassLoader(Key.create(correctedPath), jarInputStream, parentClassloader);
    }

    public List<InputStream> getResourcesAsStream(Pattern pattern) {
        if (pattern == null) {
            return Collections.singletonList(emptyByteStream());
        }

        List<InputStream> inputStreams = new ArrayList<>();

        for (String pathOfResource : availableResourceFiles) {
            Matcher matcher = pattern.matcher(pathOfResource);

            if (matcher.matches()) {
                byte[] resourceFromCache = getResourceFromCache(pathOfResource);
                inputStreams.add(new ByteArrayInputStream(resourceFromCache));
            }
        }

        return inputStreams;
    }

    private ByteArrayInputStream emptyByteStream() {
        return new ByteArrayInputStream(new byte[0]);
    }

    private byte[] getResourceFromCache(String name) {
        try {
            FileObject fileObject = getResourceFileObject(name);

            if (fileObject.exists()) {
                byte[] fileContent = fileObject.getContent().getByteArray();
                return fileContent != null ? fileContent : new byte[0];
            } else {
                return new byte[0];
            }

        } catch (IOException | InvalidFileNameException ex) {
            return new byte[0];
        }
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (!rawClassCache.containsKey(name)) {
            throw new ClassNotFoundException(name);
        }

        byte[] rawBytes = rawClassCache.remove(name); // Remove the loaded class from the cache

        // found the raw class, define it
        if (rawBytes != null) {
            return defineClass(name, rawBytes, 0, rawBytes.length);
        }

        throw new ClassNotFoundException(name);
    }

    @Override
    public URL getResource(String name) {
        try {
            FileObject fileObject = getResourceFileObject(name);

            if (fileObject.exists()) {
                return fileObject.getURL();
            } else {
                return null;
            }
        } catch (InvalidFileNameException | FileSystemException e) {
            return null;
        }
    }

    @Override
    public Enumeration<URL> getResources(String name) {
        ClassLoader parent = getParent();
        List<URL> tmp = new ArrayList<>();

        try {
            Enumeration<URL> resources = parent.getResources(name);
            while (resources.hasMoreElements()) {
                tmp.add(resources.nextElement());
            }
        } catch (IOException ignored) {
        }

        try {
            FileObject resourceFileObject = getResourceFileObject(name);
            if (resourceFileObject.exists()) {
                tmp.add(resourceFileObject.getURL());
            }
        } catch (InvalidFileNameException | FileSystemException ignored) {
        }

        return tmp.isEmpty() ? null : Collections.enumeration(tmp);
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        if (name == null) {
            return emptyByteStream();
        }

        return new ByteArrayInputStream(getResourceFromCache(name));
    }

    public Manifest getManifest() {
        return new Manifest(manifest);
    }
}
