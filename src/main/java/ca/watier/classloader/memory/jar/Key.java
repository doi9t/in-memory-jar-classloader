/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.classloader.memory.jar;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * A simple class that will be used in a {@link Map} to store the {@link ClassLoader}
 */
public class Key implements Serializable {
    private static final long serialVersionUID = 7437255088897011059L;
    private final String key;

    public Key() {
        this.key = UUID.randomUUID().toString();
    }

    /**
     * Create a new key, can be any string.
     */
    public Key(String key) {
        this.key = key;
    }

    public static Key nullKey() {
        return create(null);
    }

    public static Key create(String key) {
        return new Key(key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key1 = (Key) o;
        return Objects.equals(key, key1.key);
    }
}
