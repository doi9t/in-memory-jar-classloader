/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.classloader.memory.jar;

import ca.watier.classloader.memory.jar.exceptions.MemoryJarClassloaderException;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import static org.assertj.core.api.Assertions.assertThat;

class InMemoryJarClassloaderIntegrationTest {

    /**
     * This test is only used to do instrumentation.
     */
    @Disabled
    @Test
    void load_100k_classes_and_20k_resources() throws MemoryJarClassloaderException, IOException {
        // given
        int givenNbOfClasses = 100000;
        int givenNbOfResources = 20000;

        Key givenKey = Key.nullKey();

        String[] classNames = new String[givenNbOfClasses];
        String[] resourceNames = new String[givenNbOfResources];

        for (int i = 0; i < givenNbOfClasses; i++) {
            classNames[i] = RandomStringUtils.randomAlphabetic(15);
        }

        for (int i = 0; i < givenNbOfResources; i++) {
            resourceNames[i] = RandomStringUtils.randomAlphabetic(15);
        }

        JarInputStream givenJarInputStream = buildJarInputStreamWithEmptyClassesAndResources(classNames, resourceNames);

        // when
        InMemoryJarClassloader.load(givenKey, givenJarInputStream, null);
    }


    @Test
    void load_classes() throws MemoryJarClassloaderException, IOException, ClassNotFoundException {
        // given
        Key givenKey = Key.nullKey();
        String givenFirstClassName = "ca.test.TestClass";
        String givenSecondClassName = "ca.test.TestClass2";
        JarInputStream givenJarInputStream = buildJarInputStreamWithEmptyClasses(givenFirstClassName, givenSecondClassName);

        // when
        ClassLoader assertedClassloader = InMemoryJarClassloader.load(givenKey, givenJarInputStream, null);

        // then
        Class<?> firstClassToAssert = Class.forName(givenFirstClassName, false, assertedClassloader);
        Class<?> secondClassToAssert = Class.forName(givenSecondClassName, false, assertedClassloader);

        assertThat(firstClassToAssert.getName()).isEqualTo(givenFirstClassName);
        assertThat(secondClassToAssert.getName()).isEqualTo(givenSecondClassName);
    }

    private JarInputStream buildJarInputStreamWithEmptyClasses(String... classNames) throws IOException {
        ByteArrayOutputStream givenJarContainerStream = new ByteArrayOutputStream();
        JarOutputStream givenJarStream = new JarOutputStream(givenJarContainerStream);

        if (classNames != null) {
            for (String name : classNames) {
                addNewClassTo(givenJarStream, name);
            }
        }

        ByteArrayInputStream givenByteArrayInputStream = new ByteArrayInputStream(givenJarContainerStream.toByteArray());
        return new JarInputStream(givenByteArrayInputStream);
    }


    private JarInputStream buildJarInputStreamWithEmptyClassesAndResources(String[] classNames, String[] resourceNames) throws IOException {
        ByteArrayOutputStream givenJarContainerStream = new ByteArrayOutputStream();
        JarOutputStream givenJarStream = new JarOutputStream(givenJarContainerStream);

        if (classNames != null) {
            for (String name : classNames) {
                addNewClassTo(givenJarStream, name);
            }
        }

        if (resourceNames != null) {
            for (String name : resourceNames) {
                addNewResourceTo(givenJarStream, name);
            }
        }

        ByteArrayInputStream givenByteArrayInputStream = new ByteArrayInputStream(givenJarContainerStream.toByteArray());
        return new JarInputStream(givenByteArrayInputStream);
    }

    private void addNewClassTo(JarOutputStream givenJarStream, String binaryName) throws IOException {
        if (givenJarStream == null || binaryName == null || binaryName.isEmpty()) {
            return;
        }

        DynamicType.Builder<Object> builder = new ByteBuddy()
                .subclass(Object.class)
                .name(binaryName);

        DynamicType.Unloaded<Object> unloadedClass = builder.make();

        byte[] classBytes = unloadedClass.getBytes();
        String binaryNameToPathAndFile = binaryName.replaceAll("\\.", "/").concat(".class");

        JarEntry zipEntry = new JarEntry(binaryNameToPathAndFile);
        givenJarStream.putNextEntry(zipEntry);
        givenJarStream.write(classBytes);
        givenJarStream.closeEntry();
    }

    @Test
    void load_ressources() throws MemoryJarClassloaderException, IOException {
        // given
        Key givenKey = Key.nullKey();
        String givenFirstResourceName = "/adsyyasd/dsasdds/test.dat";
        String givenSecondResourceName = "/a/b/c/d/e.txt";

        JarInputStream jarInputStream = buildJarInputStreamWithEmptyResources(givenFirstResourceName, givenSecondResourceName);

        // when
        ClassLoader assertedClassloader = InMemoryJarClassloader.load(givenKey, jarInputStream, null);

        // then
        InputStream firstStreamToAssert = assertedClassloader.getResourceAsStream(givenFirstResourceName);
        InputStream secondStreamToAssert = assertedClassloader.getResourceAsStream(givenSecondResourceName);

        assertThat(firstStreamToAssert).isNotEmpty();
        assertThat(secondStreamToAssert).isNotEmpty();
    }

    private JarInputStream buildJarInputStreamWithEmptyResources(String... resourcesNames) throws IOException {
        ByteArrayOutputStream givenJarContainerStream = new ByteArrayOutputStream();
        JarOutputStream givenJarStream = new JarOutputStream(givenJarContainerStream);

        if (resourcesNames != null) {
            for (String name : resourcesNames) {
                addNewResourceTo(givenJarStream, name);
            }
        }

        ByteArrayInputStream givenByteArrayInputStream = new ByteArrayInputStream(givenJarContainerStream.toByteArray());
        return new JarInputStream(givenByteArrayInputStream);
    }

    private void addNewResourceTo(JarOutputStream givenJarStream, String pathAndFile) throws IOException {
        if (givenJarStream == null || pathAndFile == null || pathAndFile.isEmpty()) {
            return;
        }

        byte[] classBytes = {0x01, 0x02, 0x03, 0x04, 0x05};
        JarEntry zipEntry = new JarEntry(pathAndFile);
        givenJarStream.putNextEntry(zipEntry);
        givenJarStream.write(classBytes);
        givenJarStream.closeEntry();
    }
}